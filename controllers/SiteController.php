<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\Session;
use app\models\LoginForm;
use app\models\Datum;

//use app\models\UserRole;
//use app\models\UserClient;
//use app\models\MClient;
use yii\helpers\ArrayHelper;
use yii\db\Query;
class SiteController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

	

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
	
    private function controlview($data){
		if($data == 0){
			return $this->redirect(['login']);
		}else if($data == 1){
			return $this->redirect(['akses']);
		}else if($data == 2){
			return $this->redirect(['dashboard']);
		}else{
			throw new \yii\web\NotFoundHttpException("Sorry page not found.");
		}
	}
	public function actionDatainput(){
		$tanggal =  date("Y/m/d");
		$datafilter=null;
		$bln = "";
		$thn = "";
		$modeldata = new Datum();
		
		if(Yii::$app->request->post("btnfilter")){
				$model = Datum::find()->all();
				$bln = Yii::$app->request->post("bln");
				$thn = Yii::$app->request->post("thn");
				$blnget = strtotime($bln);
				
				$thnget = strtotime($thn);
				$year = date('Y',$thnget);
				$month = date('m',$blnget);
	
				
				$query = new Query;
				$query->select([
						'*',
						])  
						->from('datum')
						->where(['tahun'=>$year,'bulan'=>$month])->all();
				$command = $query->createCommand();
				$datafilter = $command->queryAll();
		}else if(Yii::$app->request->post("btnproses")){
			
		
		}else if ($modeldata->load(Yii::$app->request->post()) && $modeldata->save()) {
			return $this->redirect(['datainput']);
		}
		return $this->render('datainput_v', [
			'tanggal'=>$tanggal,
			'bln'=>$bln,
			'thn'=>$thn,
			'modeldata'=>$modeldata,
			'model'=>$datafilter,
			'title' => "SISTEM PENGAMBILAN KEPUTUSAN",
		]);	
	
	}
    public function actionIndex()
    {
		$data = Yii::$app->sess->checksession();
		$this->controlview($data);
	}

    
     public function actionLogin()
    {	
        $data = Yii::$app->sess->checksession();
		if($data == 1){//1
			return $this->redirect(['akses']);
		}else if($data == 2){
			return $this->redirect(['dashboard']);
		}else if($data == 0){
			$this->layout = 'themelogin';
			$model = new LoginForm();
			if (Yii::$app->request->post()) {
				$username = Yii::$app->request->post('LoginForm')['username'];
				$password = Yii::$app->request->post('LoginForm')['password'];
				
				$userdata =  Yii::$app->db->createCommand("SELECT * FROM akses WHERE username='".$username."' AND password='".$password."'")->queryAll();
			
				if($userdata != "" && $userdata!= null){
					Yii::$app->sess->regissess($userdata);
					return $this->redirect(['dashboard']);
				}else{
					Yii::$app->session->setFlash('error', "Username atau Password salah");
					return $this->redirect(['login']);
				}
			}else{
				return $this->render('login', [
					'model' => $model,
				]);
			}
		}else{
			throw new \yii\web\NotFoundHttpException("Sorry page not found.");
		}
	}
     
	public function actionAkses(){
		$data = Yii::$app->sess->checksession();
		if($data == 0){
			return $this->redirect(['login']);
		}else if($data == 2){
			return $this->redirect(['dashboard']);
		}else if($data == 1){
			$userdata = Yii::$app->sess->checkgetsess("userdata");
			$this->layout = 'themelogin';
			foreach($userdata as $data){
				$Kode_Karyawan=$data["Kode_Karyawan"];
				$Nama_Karyawan=$data["Nama_Karyawan"];
				}
			$userclient = Userclient::find()->where(['id_user'=>$Kode_Karyawan,'aktif'=>'Y'])->all();
			$client_aktif = array();foreach($userclient as $row){$client_aktif[]=$row->id_client;}
			$client = MClient::find()->where(['id_client'=>$client_aktif])->all();$listclient=ArrayHelper::map($client,'id_client','client');
			
			if (Yii::$app->request->post('client') != null && Yii::$app->request->post('role') !=null) {
				$client = Yii::$app->request->post('client');
				$role = Yii::$app->request->post('role');
				Yii::$app->sess->regiswithname('client',$client);
				Yii::$app->sess->regiswithname('role',$role);
				
				$dataclient = Yii::$app->sess->checkgetsess('client');
				$datarole = Yii::$app->sess->checkgetsess('role');
				
				$query = new Query;
                    $query->select([
                            'c.*',
                            'a.id_user_client',
                            'a.id_user_role'
                            ])  
                            ->from('akses AS a')
                            ->Join(	'INNER JOIN', 
                                'akses_det AS b',
                                'a.id_akses = b.id_akses'
                            )->Join('INNER JOIN', 
                            'm_modul AS c',
                            'b.id_modul = c.id_modul'
                             )->where(['a.id_user_client'=>$dataclient,'a.id_user_role'=>$datarole]
                            );
				$command = $query->createCommand();
				$menu = $command->queryAll();
				if($menu == null || $menu == ""){
					Yii::$app->sess->regiswithname('menu',null);
					Yii::$app->sess->regiswithname('submenu',null);
					Yii::$app->sess->regiswithname('subsubmenu',null);
				}else{
					
					Yii::$app->sess->regiswithname('menu',$menu);
					$datamenu = $menu;
						foreach($menu as $datamenu){
						$paramIdModul[] = $datamenu['id_modul'];
					}
					$query = new Query;
                    $query->select([
                        'b.id_modul_det',
                        'b.id_modul',
                        'b.modul',
                        'b.link',
                        'b.id_modul',
                        'b.parent'
                        ])  
					->from('m_modul AS a')
					->Join(	'RIGHT JOIN', 
						'm_modul_det AS b',
						'a.id_modul = b.id_modul'
					)->where(['b.aktif'=>'1']
					)->andWhere(['IN','b.id_modul',$paramIdModul]);
					$command = $query->createCommand();
					$submenu = $command->queryAll();
					if($submenu == null || $submenu == ""){
                            Yii::$app->sess->regiswithname('submenu',null);
							Yii::$app->sess->regiswithname('subsubmenu',null);
					}else{
						Yii::$app->sess->regiswithname('submenu',$submenu);
						foreach($submenu as $datasubmenu){
							$id_modul_det[] = $datasubmenu['id_modul_det'];
						}
						$query = new Query;
						$query->select([
							'b.id_modul_det',
							'b.modul',
							'b.link',
							])  
							->from('m_modul_det AS a')
							->Join(	'LEFT JOIN', 
								'm_modul_det_det AS b',
								'a.id_modul_det = b.id_modul_det'
							)->where(['b.aktif'=>'1','a.aktif'=>'1']
							)->andWhere(['IN','b.id_modul_det',$id_modul_det]);
							$command = $query->createCommand();
							$subsubmenu = $command->queryAll();
							if($subsubmenu == null || $subsubmenu == ""){
								Yii::$app->sess->regiswithname('subsubmenu',null);
							}else{
								Yii::$app->sess->regiswithname('subsubmenu',$subsubmenu);
							}			   
					}
				}
				return $this->redirect(['dashboard']);
			}else{
				return $this->render('akses',[
					 'listclient' => $listclient,
					 'userdata'=>$Nama_Karyawan
				]);		
			}
		}else{
			throw new \yii\web\NotFoundHttpException("Sorry page not found.");
		}
	}

     public function actionLogout()
    {
        $session = Yii::$app->session;
        $session->open();
        if(Yii::$app->request->post()){
            if($session->get('userdata') != null){
                $session->destroy();
                return $this->redirect(['login']);
            }else{
                throw new \yii\web\NotFoundHttpException("Sorry page not found.");
            }
        }else{
            throw new \yii\web\NotFoundHttpException("Sorry page not found.");
        }
    }

	public function actionDashboard(){
		$userdata = Yii::$app->sess->checkgetsess("userdata");
		$data = Yii::$app->sess->checksession();
		if($data == 1){
			return $this->redirect(['akses']);
		}else if($data == 0){
			return $this->redirect(['login']);
		}
		
		return $this->render('index',[
			'userdata'=>$userdata 
		]);

	}

 public function  actionProsesfuzzy(){
	print_r("daat");
}
}
