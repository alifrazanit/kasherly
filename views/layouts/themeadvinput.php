<?php
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset_datatableadv;
use yii\helpers\Url;
use yii\web\Session;

$session = Yii::$app->session;
/*$userdata = $session->get('userdata');
foreach($userdata as $data){
	$Kode_Karyawan=$data["Kode_Karyawan"];
	$Nama_Karyawan=$data["Nama_Karyawan"];
	$nik=$data["nik"];
}*/
AppAsset_datatableadv::register($this);
                
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>RU</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">SINAR RODA UTAMA</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs"> <?php#$Nama_Karyawan?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <!--img src="theme/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"-->

                <p>
                  <?php#$Nama_Karyawan?>
                  <small><?php#$nik?></small>
                </p>
              </li>
              <li class="user-footer">
			  <div class="pull-left">
                  <a href="http://10.10.20.231/sirup/masterkaryawan/update/<?php# $Kode_Karyawan;?>" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
				<a href="<?= Url::base().'/site/logout';?>" class="btn btn-default btn-flat" data-method="post"> Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
		<li class="header">MAIN NAVIGATION</li>
		<?php
			
			if($session->get('menu') != null){
				$menu = $session->get('menu');
				for($i=0;$i<sizeof($menu);$i++){
					 if($menu[$i]['parent'] == 1){
		?>
				<li class="treeview">
					<a href="#">
						<i class="<?=$menu[$i]['icon']?>"></i> <span><?=$menu[$i]['modul']?></span>
						<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<?php
						$submenu = $session->get('submenu');
							foreach($submenu as $smenu){
								if($smenu['id_modul'] == $menu[$i]['id_modul']){
									if($smenu['parent'] == '0'){
									?>
									<li>
										<a href="<?= Url::base().'/'.$smenu['link'];?>"><?= $smenu['modul'];?></a>
									</li>
									<?php	
									}else if($smenu['parent'] == '1'){
										
									?>
										 <li class="treeview">
										  <a href="#">
											<span><?= $smenu['modul'];?></span>
											<span class="pull-right-container">
											  <i class="fa fa-angle-left pull-right"></i>
											</span>
										  </a>
										  <ul class="treeview-menu">
										  <?php
										  $subsubmenu = $session->get('subsubmenu');
											for($j=0;$j<sizeof($subsubmenu);$j++){
												?>
												<li><a href="<?=$subsubmenu[$j]["link"];?>"><?=$subsubmenu[$j]["modul"];?></a></li>
												<?php
											
											}
										  ?>
										  
										  
											
										  </ul>
										</li>
									<?php	
									}
								}
							}
						?>
					
					</ul>
				</li>
		<?php
					 }else if($menu[$i]['parent'] == 0){
					?>
						<li>
							<a href="<?=$menu[$i]['link']?>"><i class="<?=$menu[$i]['icon']?>"></i> <?=$menu[$i]['modul']?></a>
						</li>
					<?php		 
					 }
				}
			}
			if($session['role'] == "1"){
		?>
			
			<li>
				<a href="<?php echo Url::base(); ?>/gii"><i class="fa fa-share-square-o"></i> Gii Tools</a>
			</li>
		<?php  
			}
		?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
 <div class="content-wrapper">
 <section class="content-header">
 <?= 
   Breadcrumbs::widget([
      'homeLink' => [ 
                      'label' => Yii::t('yii', 'Dashboard'),
                      'url' => Url::base().'/site/dashboard',
                 ],
      'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
   ]) 
?>
 </section>

    <section class="content">
      <?= $content ?>
    </section>
    <!-- /.content -->
  </div>
  <footer class="main-footer">
    <strong>Copyright &copy; 2019 <a href="http://10.10.20.231/sirup/">PT. Sinar Roda Utama</a>.</strong> All rights
    reserved.
  </footer>

</div>
<!-- ./wrapper -->
<?php $this->endBody() ?>
<script>
  $(function () {
	  $('#example1').DataTable({
      "scrollY": 250
    });
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
	  
	  
	  
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
  /*  $("#test2").inputmask("y", {
    alias: "date",    
    placeholder: "yyyy",
    yearrange: { minyear: 1900, maxyear: (new Date()).getFullYear() }});
*/
    $('#datemask').inputmask('y', { 
    alias: "date",    
    placeholder: "yyyy",
    yearrange: { minyear: 1900, maxyear: (new Date()).getFullYear() }});
    $('#datemaskbln').inputmask('m', { 
    alias: "date",    
    placeholder: "m"});
    
    //$('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, locale: { format: 'MM/DD/YYYY hh:mm A' }})
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>
<?php $this->endPage() ?>