<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Datum */

$this->title = 'Update Datum: ' . $model->id_datum;
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_datum, 'url' => ['view', 'id' => $model->id_datum]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="datum-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
