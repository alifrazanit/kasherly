<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Datum */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="datum-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'no')->textInput() ?>

    <?= $form->field($model, 'tahun')->textInput() ?>

    <?= $form->field($model, 'bulan')->textInput() ?>

    <?= $form->field($model, 'kode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'edu')->textInput() ?>

    <?= $form->field($model, 'vi')->textInput() ?>

    <?= $form->field($model, 'rrs')->textInput() ?>

    <?= $form->field($model, 'con')->textInput() ?>

    <?= $form->field($model, 'ci')->textInput() ?>

    <?= $form->field($model, 'acp')->textInput() ?>

    <?= $form->field($model, 'ys')->textInput() ?>

    <?= $form->field($model, 'ar')->textInput() ?>

    <?= $form->field($model, 'bvr')->textInput() ?>

    <?= $form->field($model, 'wa')->textInput() ?>

    <?= $form->field($model, 'vp')->textInput() ?>

    <?= $form->field($model, 'dc')->textInput() ?>

    <?= $form->field($model, 'atc')->textInput() ?>

    <?= $form->field($model, 'qw')->textInput() ?>

    <?= $form->field($model, 'ws')->textInput() ?>

    <?= $form->field($model, 'ri')->textInput() ?>

    <?= $form->field($model, 'ctv')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
