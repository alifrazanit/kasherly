<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DatumS */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="datum-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_datum') ?>

    <?= $form->field($model, 'no') ?>

    <?= $form->field($model, 'tahun') ?>

    <?= $form->field($model, 'bulan') ?>

    <?= $form->field($model, 'kode') ?>

    <?php // echo $form->field($model, 'edu') ?>

    <?php // echo $form->field($model, 'vi') ?>

    <?php // echo $form->field($model, 'rrs') ?>

    <?php // echo $form->field($model, 'con') ?>

    <?php // echo $form->field($model, 'ci') ?>

    <?php // echo $form->field($model, 'acp') ?>

    <?php // echo $form->field($model, 'ys') ?>

    <?php // echo $form->field($model, 'ar') ?>

    <?php // echo $form->field($model, 'bvr') ?>

    <?php // echo $form->field($model, 'wa') ?>

    <?php // echo $form->field($model, 'vp') ?>

    <?php // echo $form->field($model, 'dc') ?>

    <?php // echo $form->field($model, 'atc') ?>

    <?php // echo $form->field($model, 'qw') ?>

    <?php // echo $form->field($model, 'ws') ?>

    <?php // echo $form->field($model, 'ri') ?>

    <?php // echo $form->field($model, 'ctv') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
