<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Datum */

$this->title = $model->id_datum;
$this->params['breadcrumbs'][] = ['label' => 'Data', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="datum-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_datum], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_datum], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_datum',
            'no',
            'tahun',
            'bulan',
            'kode',
            'edu',
            'vi',
            'rrs',
            'con',
            'ci',
            'acp',
            'ys',
            'ar',
            'bvr',
            'wa',
            'vp',
            'dc',
            'atc',
            'qw',
            'ws',
            'ri',
            'ctv',
        ],
    ]) ?>

</div>
