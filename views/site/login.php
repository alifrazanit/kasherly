<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div class="login-box">
  <div class="login-logo">
    Sistem SPK
  </div>
  <?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <h4><i class="icon fa fa-check"></i>Saved!</h4>
         <?= Yii::$app->session->getFlash('success') ?>
    </div>
<?php endif; ?>

<?php if (Yii::$app->session->hasFlash('error')): ?>
    <div class="alert alert-danger alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <h4><i class="icon fa fa-exclamation-circle"></i>Failed!</h4>
         <?= Yii::$app->session->getFlash('error') ?>
    </div>
<?php endif; ?>
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <?php $form = ActiveForm::begin();?>
      <div class="form-group has-feedback">
		<?= $form->field($model, 'username')->textInput(['autofocus' => true,'class'=>'form-control','placeholder'=>'Username']) ?>
      </div>
      <div class="form-group has-feedback">
		<?= $form->field($model, 'password')->passwordInput(['class'=>'form-control','placeholder'=>'Password']) ?>
      </div>
      <div class="row">
        
        <!-- /.col -->
        <div class="col-xs-4">
		<?= Html::submitButton('Login', ['class' => '"btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
        </div>
        <!-- /.col -->
      </div>
    <?php ActiveForm::end(); ?>

    
  </div>
  <!-- /.login-box-body -->
</div>