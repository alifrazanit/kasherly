<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$js=<<<js
$(document).ready(function() {
  var getUrl = window.location;
  var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
  
  $('select#clientoption').on('change', function() {
    var formData = {
      'datapost':this.value
    };
  
     $.ajax({
      url: baseUrl + '/site/getrole',
      dataType:'json',
      type: 'ajax',
      method:'post',
      async:false,
      data: formData,
      success: function(data){
      
        $('#role').empty();
        $(data['data']).each(function()
        {
          if(this.id_user_client != null && this.id_role != null){
            var option = $('<option>');
            option.attr('value', this.id_role).text(this.role);
            $('#role').append(option);
          }    
        });
      },
      error: function(err){
        console.log(err);
        alert(JSON.stringify(err));
      }
    })
  });   
 
});
js;
$this->registerJs($js);
?>
<div class="login-box">
  <div class="login-logo">
    <a href="#">SRU - TAM</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">hai <b><?= $userdata;?></b>, Silahkan tentukan user client dan role Anda.</p>

    <?php $form = ActiveForm::begin();?>
      <div class="form-group has-feedback">
		<?= Html::dropDownList('client', 'Client',$listclient,['class'=>'input form-control','id'=>'clientoption','prompt'=>'===PILIH CLIENT===']) ?>
      </div>
      <div class="form-group has-feedback">
		<label>Role</label>
		<select name="role" id="role" class="input form-control">
			<option>== PILIH ROLE ==</option>
		</select>
      </div>
      <div class="row">
        
        <!-- /.col -->
        <div class="col-xs-4">
		<?= Html::submitButton('Login', ['class' => '"btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
        </div>
        <!-- /.col -->
      </div>
    <?php ActiveForm::end(); ?>

    
  </div>
  <!-- /.login-box-body -->
</div>