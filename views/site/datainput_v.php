<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->params['breadcrumbs'][] = ['label' => 'Data Input', 'url' => Url::base().'/site/datainput'];

?>

<style>
    .jarak {
        margin-top: 1%;
    }

    .jarakmasuk {
        padding-left: 30px;
    }
</style>
<div class="box">
    <div class="box-header">
        <center>
            <h3 class="box-title"><?=$title?></h3>
        </center>
    </div>
    <div class="box-body">
    <div class="row">
    <div class="col-md-12 jarakmasuk">
                <p>Tanggal : <?= $tanggal;?></p>
            </div>    
    
    </div>
    <?php $form = ActiveForm::begin(); ?>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Tahun:</label>
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
                            </div>
							<input type="date" class="form-control" id="datemask" name="thn" value="<?=$thn?>">
						</div>
						<small>Min:1900, Max:2099</small>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Bulan:</label>
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="date" class="form-control" id="datemaskbln" name="bln" value="<?=$bln?>">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
                    <input type="submit" name="btnfilter" value="Filter" class="btn btn-success"/>
					<a href="<?=Url::base().'/site/prosesfuzzy' ?>"  class="btn btn-info">Proses</a>
					
                    </div>
				</div>
			</div>
		<?php ActiveForm::end(); ?>
        <div class="row">
				<div class="col-md-12">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                    Tambah data
                </button>
				</div>
			</div>
        <div class="row">
            <div class="col-md-12 jarak">
                <table id="example1" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tahun</th>
                            <th>Bulan</th>
                            <th>Kode</th>
                            <th>Edu</th>
                            <th>VI</th>
                            <th>RRS</th>
                            <th>Con</th>
                            <th>CI</th>
                            <th>Acp</th>
                            <th>YS</th>
                            <th>AR</th>
                            <th>Bvr</th>
                            <th>WA</th>
                            <th>VP</th>
                            <th>Dc</th>
                            <th>Atc</th>
                            <th>QW</th>
                            <th>WS</th>
                            <th>RI</th>
                            <th>Ctv</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
						if($model != null){
						$i = 1; 
						foreach($model as $value){
					?>
                        <tr>
                            <td><?= $i;?></td>
                            <td><?= $value["tahun"];?></td>
                            <td><?= $value["bulan"];?></td>
                            <td><?= $value["kode"];?></td>
                            <td><?= $value["edu"];?></td>
                            <td><?= $value["vi"];?></td>
                            <td><?= $value["rrs"];?></td>
                            <td><?= $value["con"];?></td>
                            <td><?= $value["ci"];?></td>
                            <td><?= $value["acp"];?></td>
                            <td><?= $value["ys"];?></td>
                            <td><?= $value["ar"];?></td>
                            <td><?= $value["bvr"];?></td>
                            <td><?= $value["wa"];?></td>
                            <td><?= $value["vp"];?></td>
                            <td><?= $value["dc"];?></td>
                            <td><?= $value["atc"];?></td>
                            <td><?= $value["qw"];?></td>
                            <td><?= $value["ws"];?></td>
                            <td><?= $value["ri"];?></td>
                            <td><?= $value["ctv"];?></td>
                        </tr>
                        <?php
						$i++;
					 	}}
					?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Default Modal</h4>
              </div>
              <div class="modal-body">
              <?php $form = ActiveForm::begin(); ?>
              <?= $form->field($modeldata, 'tahun')->textInput() ?>
              <?= $form->field($modeldata, 'bulan')->textInput() ?>
              <?= $form->field($modeldata, 'kode')->textInput() ?>
              <?= $form->field($modeldata, 'edu')->textInput() ?>
              <?= $form->field($modeldata, 'vi')->textInput() ?>
              <?= $form->field($modeldata, 'rrs')->textInput() ?>
              <?= $form->field($modeldata, 'con')->textInput() ?>
              <?= $form->field($modeldata, 'ci')->textInput() ?>
              <?= $form->field($modeldata, 'acp')->textInput() ?>
              <?= $form->field($modeldata, 'ys')->textInput() ?>
              <?= $form->field($modeldata, 'ar')->textInput() ?>
              <?= $form->field($modeldata, 'bvr')->textInput() ?>
              <?= $form->field($modeldata, 'wa')->textInput() ?>
              <?= $form->field($modeldata, 'vp')->textInput() ?>
              <?= $form->field($modeldata, 'dc')->textInput() ?>

              <?= $form->field($modeldata, 'atc')->textInput() ?>
              <?= $form->field($modeldata, 'qw')->textInput() ?>
              <?= $form->field($modeldata, 'ws')->textInput() ?>
              <?= $form->field($modeldata, 'ri')->textInput() ?>
              <?= $form->field($modeldata, 'ctv')->textInput() ?>
              <input type="submit" name="simpandata" class="btn btn-primary"/>
              <?php ActiveForm::end(); ?>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>