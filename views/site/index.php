<h1>Dashboard</h1>
<div class="row">
	<div class="col-md-4">
		<div class="nav-tabs-custom">
			<!-- Tabs within a box -->
			<ul class="nav nav-tabs pull-right">
				<li class="active"><a href="#revenue-chart" data-toggle="tab">Ranking</a></li>
				<li class="pull-left header"><i class="fa fa-inbox"></i> SPK</li>
			</ul>
			<div class="tab-content no-padding">
				<!-- Morris chart - Sales -->
				<div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
			</div>
		</div>
	</div>
    <div class="col-md-4">
		<div class="nav-tabs-custom">
			<!-- Tabs within a box -->
			<ul class="nav nav-tabs pull-right">
				<li class="active"><a href="#revenue-chart" data-toggle="tab">Ranking</a></li>
				<li class="pull-left header"><i class="fa fa-inbox"></i> SPK</li>
			</ul>
			<div class="tab-content no-padding">
				<!-- Morris chart - Sales -->
				<div class="chart tab-pane active" id="sales-chart" style="position: relative; height: 300px;"></div>
			</div>
		</div>
	</div>
	
    <div class="col-md-4">
		<div class="box box-solid bg-green-gradient">
			<div class="box-header">
				<i class="fa fa-calendar"></i>

				<h3 class="box-title">Calendar</h3>
				<!-- tools box -->
				<div class="pull-right box-tools">
					<!-- button with a dropdown -->
					<button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i
							class="fa fa-minus"></i>
					</button>
					<button type="button" class="btn btn-success btn-sm" data-widget="remove"><i
							class="fa fa-times"></i>
					</button>
				</div>
				<!-- /. tools -->
			</div>
			<!-- /.box-header -->
			<div class="box-body no-padding">
				<!--The calendar -->
				<div id="calendar" style="width: 100%"></div>
			</div>
		</div>
	</div>
</div>