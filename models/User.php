<?php

namespace app\models;
use app\models\MasterKaryawan;
class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id;
    public $Kode_Karyawan;

   
    public $authKey;
    public $accessToken;


    public $nik;
    public $Username;
    public $Password;
    public $role;
    public $Nama_Karyawan;
	public $setup_user;
	public $notulen;
    public $Tipe_User;
    public $Kode_Cabang;
    public $marketing;
    public $status_marketing;
    public $Kode_Atasan;
    public $Category;
    public $Category_Tracking;
    public $Category_Konsep_Komisi;
    public $Konsep_Komisi;
    public $Category_AR_Analysis;
    public $cat_sp;
    public $cat_coll;
    public $nama_erp;
    public $no_dept;
	public $id_dept;
	public $kd_sales;
	public $atk;
	public $kirim_dok;
    public $email;
    public $no_telp;
    public $sales_tracking;
    public $ip_address;
    public $active;
    public $kode_sales;
    public $alamat;
    public $ktp;
	public $place;
	public $divisi;
	public $kode_divisi;
	public $id_m_jabatan;
	public $id_rm;
	public $report_pms;
	public $wilayah;
	public $usergroup;
	public $foto;
/*
    private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'admin',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id' => '101',
            'username' => 'demo',
            'password' => 'demo',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
        ],
    ];
*/

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */

    public static function findByUsername($Username)
    {
        //mencari user login berdasarkan username dan hanya dicari 1.
        
        $user= Masterkaryawan::find()->where(['username'=>$Username])->one();
        if($user!= null){
            return new static($user);
        }
       
        return null;
    }
   
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->Password === $password;
    }
}
