<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Datum;

/**
 * DatumS represents the model behind the search form of `app\models\Datum`.
 */
class DatumS extends Datum
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_datum', 'tahun', 'bulan'], 'integer'],
            [['kode'], 'safe'],
            [['edu', 'vi', 'rrs', 'con', 'ci', 'acp', 'ys', 'ar', 'bvr', 'wa', 'vp', 'dc', 'atc', 'qw', 'ws', 'ri', 'ctv'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Datum::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_datum' => $this->id_datum,
            'no' => $this->no,
            'tahun' => $this->tahun,
            'bulan' => $this->bulan,
            'edu' => $this->edu,
            'vi' => $this->vi,
            'rrs' => $this->rrs,
            'con' => $this->con,
            'ci' => $this->ci,
            'acp' => $this->acp,
            'ys' => $this->ys,
            'ar' => $this->ar,
            'bvr' => $this->bvr,
            'wa' => $this->wa,
            'vp' => $this->vp,
            'dc' => $this->dc,
            'atc' => $this->atc,
            'qw' => $this->qw,
            'ws' => $this->ws,
            'ri' => $this->ri,
            'ctv' => $this->ctv,
        ]);

        $query->andFilterWhere(['like', 'kode', $this->kode]);

        return $dataProvider;
    }
}
