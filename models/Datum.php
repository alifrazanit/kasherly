<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "datum".
 *
 * @property int $id_datum
 * @property int $no
 * @property int $tahun
 * @property int $bulan
 * @property string $kode
 * @property double $edu
 * @property double $vi
 * @property double $rrs
 * @property double $con
 * @property double $ci
 * @property double $acp
 * @property double $ys
 * @property double $ar
 * @property double $bvr
 * @property double $wa
 * @property double $vp
 * @property double $dc
 * @property double $atc
 * @property double $qw
 * @property double $ws
 * @property double $ri
 * @property double $ctv
 */
class Datum extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'datum';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'tahun', 'bulan'], 'integer'],
            [['edu', 'vi', 'rrs', 'con', 'ci', 'acp', 'ys', 'ar', 'bvr', 'wa', 'vp', 'dc', 'atc', 'qw', 'ws', 'ri', 'ctv'], 'number'],
            [['kode'], 'string', 'max' => 4],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_datum' => 'Id Datum',
            'tahun' => 'Tahun',
            'bulan' => 'Bulan',
            'kode' => 'Kode',
            'edu' => 'Edu',
            'vi' => 'Vi',
            'rrs' => 'Rrs',
            'con' => 'Con',
            'ci' => 'Ci',
            'acp' => 'Acp',
            'ys' => 'Ys',
            'ar' => 'Ar',
            'bvr' => 'Bvr',
            'wa' => 'Wa',
            'vp' => 'Vp',
            'dc' => 'Dc',
            'atc' => 'Atc',
            'qw' => 'Qw',
            'ws' => 'Ws',
            'ri' => 'Ri',
            'ctv' => 'Ctv',
        ];
    }
}
