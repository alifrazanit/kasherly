<?php
namespace app\components;
use Yii;
use yii\helpers\Html;
use yii\web\Session;
use yii\base\Component;
use yii\db\Query;
class Pengkodean extends Component{
	public $no_tiket;
	public $no_wo;
	public function init(){
        parent::init();
        $this->no_wo= null;
		$this->no_tiket= null;
    }
	public function pengkodeanWO($data){
		$kode_kunjungan = $data["kode_kunjungan"];
		if ($kode_kunjungan < 10) {
			$datakunjungan = "0" . $kode_kunjungan;
		}
		$bln = date("m");
		$thn = date("Y");
		$dateconv = date("Y-m-d");
		$query = new Query;
		$query->select(['no_wo as c'])->from('work_order')->where(['YEAR(datecreated)' => $thn])
		->andWhere(['and',
           ['MONTH(datecreated)' => $bln]
       ])
		->orderBy([
			'no_wo' => SORT_DESC
		])->limit(1);
		$thnkcl =date("y");
        $command = $query->createCommand();
        $datawo = $command->queryAll();
		if($datawo == null){
			$tmpkode = "0001WO".$datakunjungan.$thnkcl.$bln;
		}else{
			$tmpkodez = $datawo[0]["c"];
			//PERSAMAAN TAHUN
			$substhn = substr($tmpkodez,8,2);
			if($substhn == $thnkcl){//jika tahun sama
				$substrbln = substr($tmpkodez,10,2); //CEK BULAN 	
				if($substrbln == $bln){
					$subs = substr($tmpkodez,0,4);
					$subs+=1;
					if($subs < 10){
						$tmpkoden = "000".$subs;
					}else if($subs < 100){
						$tmpkoden = "00".$subs;
					}else if($subs < 1000){
						$tmpkoden = "0".$subs;
					}else if($subs < 10000){
						$tmpkoden = $subs;
					}
					$tmpkode = $tmpkoden."WO".$datakunjungan.$thnkcl.$bln;
				}else{//bulan beda
					$tmpkode = "0001WO".$datakunjungan.$thnkcl.$bln;
				}
			}else{
				$tmpkode = "0001WO".$datakunjungan.$thnkcl.$bln;
			}
		}
		return $tmpkode;
	}
}
?>