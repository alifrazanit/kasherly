<?php
namespace app\components;
use Yii;
use yii\helpers\Html;
use yii\web\Session;
use yii\base\Component;

class Sess extends Component{
    public $sess_data;
	public $sess_akses;
	
	public function init(){
        parent::init();
        $this->sess_data= null;
    }
	public function checksession(){
		$session = Yii::$app->session;
		$userdata = $session->get('userdata');
		if($userdata == null){
			return 0;
		}else{
			$role = $session->get('role');
			if($role  == null){
				return 2; //NANTI GANTI SETELAH role ada
			}else{
				return 2;
			}
		}
	}
	
	public function checkgetsess($sess){
		$session = Yii::$app->session;
		$data = $session->get($sess);
		if($data != null){
			return $data;
		}else{
			return null;
		}
	}
	public function regiswithname($name,$data){
		$session = Yii::$app->session;
		$session->set($name, $data);
	}
	public function regissess($userdata){
		$session = Yii::$app->session;
		$this->sess_data= $userdata;
		$session->set('userdata', $this->sess_data);
	}
	public function destroysess(){
		$session = Yii::$app->session;
		$session->destroy();
	}
	public function removesess($sess){
		$session = Yii::$app->session;
		$data = $session->get($sess);
		if($data != null){
			$session->remove($data);
			return true;
		}else{
			return false;
		}
	}
	
}
?>