-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.34-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for thesis
CREATE DATABASE IF NOT EXISTS `thesis` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `thesis`;

-- Dumping structure for table thesis.akses
CREATE TABLE IF NOT EXISTS `akses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(120) DEFAULT NULL,
  `password` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table thesis.akses: ~0 rows (approximately)
/*!40000 ALTER TABLE `akses` DISABLE KEYS */;
INSERT INTO `akses` (`id`, `username`, `password`) VALUES
	(1, 'sherly', 'sherly');
/*!40000 ALTER TABLE `akses` ENABLE KEYS */;

-- Dumping structure for table thesis.datum
CREATE TABLE IF NOT EXISTS `datum` (
  `id_datum` int(11) NOT NULL AUTO_INCREMENT,
  `tahun` year(4) DEFAULT NULL,
  `bulan` int(11) DEFAULT NULL,
  `kode` varchar(4) DEFAULT NULL,
  `edu` double DEFAULT NULL,
  `vi` double DEFAULT NULL,
  `rrs` double DEFAULT NULL,
  `con` double DEFAULT NULL,
  `ci` double DEFAULT NULL,
  `acp` double DEFAULT NULL,
  `ys` double DEFAULT NULL,
  `ar` double DEFAULT NULL,
  `bvr` double DEFAULT NULL,
  `wa` double DEFAULT NULL,
  `vp` double DEFAULT NULL,
  `dc` double DEFAULT NULL,
  `atc` double DEFAULT NULL,
  `qw` double DEFAULT NULL,
  `ws` double DEFAULT NULL,
  `ri` double DEFAULT NULL,
  `ctv` double DEFAULT NULL,
  PRIMARY KEY (`id_datum`),
  UNIQUE KEY `kode` (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table thesis.datum: ~3 rows (approximately)
/*!40000 ALTER TABLE `datum` DISABLE KEYS */;
INSERT INTO `datum` (`id_datum`, `tahun`, `bulan`, `kode`, `edu`, `vi`, `rrs`, `con`, `ci`, `acp`, `ys`, `ar`, `bvr`, `wa`, `vp`, `dc`, `atc`, `qw`, `ws`, `ri`, `ctv`) VALUES
	(1, '2018', 12, 'K001', 2.9, 16, 2, 4, 2.5, 5, 5, 15, 8.2, 60, 4.5, 2.6, 50, 120, 3.5, 2.2, 2.8),
	(2, '2018', 12, 'K002', 2, 4, 2, 4, 1, 5, 2, 4, 5, 2, 1, 3, 4, 3, 4, 21, 1);
/*!40000 ALTER TABLE `datum` ENABLE KEYS */;

-- Dumping structure for table thesis.hasilfuzzy
CREATE TABLE IF NOT EXISTS `hasilfuzzy` (
  `id_hasil` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(4) DEFAULT NULL,
  `tahun` year(4) DEFAULT NULL,
  `bulan` int(2) DEFAULT NULL,
  `edu` double DEFAULT NULL,
  `vi` double DEFAULT NULL,
  `rrs` double DEFAULT NULL,
  `con` double DEFAULT NULL,
  `ci` double DEFAULT NULL,
  `acp` double DEFAULT NULL,
  `ys` double DEFAULT NULL,
  `ar` double DEFAULT NULL,
  `bvr` double DEFAULT NULL,
  `wa` double DEFAULT NULL,
  `vp` double DEFAULT NULL,
  `dc` double DEFAULT NULL,
  `atc` double DEFAULT NULL,
  `qw` double DEFAULT NULL,
  `ws` double DEFAULT NULL,
  `ri` double DEFAULT NULL,
  `ctv` double DEFAULT NULL,
  PRIMARY KEY (`id_hasil`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;

-- Dumping data for table thesis.hasilfuzzy: ~2 rows (approximately)
/*!40000 ALTER TABLE `hasilfuzzy` DISABLE KEYS */;
INSERT INTO `hasilfuzzy` (`id_hasil`, `kode`, `tahun`, `bulan`, `edu`, `vi`, `rrs`, `con`, `ci`, `acp`, `ys`, `ar`, `bvr`, `wa`, `vp`, `dc`, `atc`, `qw`, `ws`, `ri`, `ctv`) VALUES
	(74, 'K001', '2018', 12, 3.5789, 4, 3, 1, 4, 4, 5, 1, 2.3594, 5, 3.7143, 3.875, 1, 4, 4, 4.3333, 3.6667),
	(75, 'K002', '2018', 12, 2.5, 1, 3, 1, 1, 4, 3, 1, 1, 1, 1, 4, 1, 4, 4, 4, 1);
/*!40000 ALTER TABLE `hasilfuzzy` ENABLE KEYS */;

-- Dumping structure for table thesis.hasilnilaikompetensi
CREATE TABLE IF NOT EXISTS `hasilnilaikompetensi` (
  `id_hasil` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(4) DEFAULT NULL,
  `bulan` int(2) DEFAULT NULL,
  `tahun` year(4) DEFAULT NULL,
  `edu` double DEFAULT NULL,
  `vi` double DEFAULT NULL,
  `rrs` double DEFAULT NULL,
  `con` double DEFAULT NULL,
  `ci` double DEFAULT NULL,
  `acp` double DEFAULT NULL,
  `ys` double DEFAULT NULL,
  `ar` double DEFAULT NULL,
  `bvr` double DEFAULT NULL,
  `wa` double DEFAULT NULL,
  `vp` double DEFAULT NULL,
  `dc` double DEFAULT NULL,
  `atc` double DEFAULT NULL,
  `qw` double DEFAULT NULL,
  `ws` double DEFAULT NULL,
  `ri` double DEFAULT NULL,
  `ctv` double DEFAULT NULL,
  PRIMARY KEY (`id_hasil`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table thesis.hasilnilaikompetensi: ~2 rows (approximately)
/*!40000 ALTER TABLE `hasilnilaikompetensi` DISABLE KEYS */;
INSERT INTO `hasilnilaikompetensi` (`id_hasil`, `kode`, `bulan`, `tahun`, `edu`, `vi`, `rrs`, `con`, `ci`, `acp`, `ys`, `ar`, `bvr`, `wa`, `vp`, `dc`, `atc`, `qw`, `ws`, `ri`, `ctv`) VALUES
	(1, 'K001', 12, '2018', 4.5, 4.5, 4, 1, 4, 4.5, 4.5, 2, 3, 4.5, 4.5, 4.5, 3, 4.5, 4.5, 4.5, 4.5),
	(2, 'K002', 12, '2018', 4, 3, 4, 1, 1, 4.5, 4, 2, 2, 2, 3, 4.5, 3, 4.5, 4.5, 4.5, 3);
/*!40000 ALTER TABLE `hasilnilaikompetensi` ENABLE KEYS */;

-- Dumping structure for table thesis.hasilnilaipf
CREATE TABLE IF NOT EXISTS `hasilnilaipf` (
  `id_hasil` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(4) DEFAULT NULL,
  `tahun` year(4) DEFAULT NULL,
  `bulan` int(2) DEFAULT NULL,
  `edu` double DEFAULT NULL,
  `vi` double DEFAULT NULL,
  `rrs` double DEFAULT NULL,
  `con` double DEFAULT NULL,
  `ci` double DEFAULT NULL,
  `acp` double DEFAULT NULL,
  `ys` double DEFAULT NULL,
  `ar` double DEFAULT NULL,
  `bvr` double DEFAULT NULL,
  `wa` double DEFAULT NULL,
  `vp` double DEFAULT NULL,
  `dc` double DEFAULT NULL,
  `atc` double DEFAULT NULL,
  `qw` double DEFAULT NULL,
  `ws` double DEFAULT NULL,
  `ri` double DEFAULT NULL,
  `ctv` double DEFAULT NULL,
  PRIMARY KEY (`id_hasil`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table thesis.hasilnilaipf: ~2 rows (approximately)
/*!40000 ALTER TABLE `hasilnilaipf` DISABLE KEYS */;
INSERT INTO `hasilnilaipf` (`id_hasil`, `kode`, `tahun`, `bulan`, `edu`, `vi`, `rrs`, `con`, `ci`, `acp`, `ys`, `ar`, `bvr`, `wa`, `vp`, `dc`, `atc`, `qw`, `ws`, `ri`, `ctv`) VALUES
	(10, 'K001', '2018', 12, 0.5789, 1, -1, -4, -1, 1, 1, -3, -1.6406, 1, 0.7143, 0.875, -2, 1, 1, 1.3333, 0.6667),
	(11, 'K002', '2018', 12, -0.5, -2, -1, -4, -4, 1, -1, -3, -3, -3, -2, 1, -2, 1, 1, 1, -2);
/*!40000 ALTER TABLE `hasilnilaipf` ENABLE KEYS */;

-- Dumping structure for table thesis.hasilnilairanking
CREATE TABLE IF NOT EXISTS `hasilnilairanking` (
  `kode` varchar(4) DEFAULT NULL,
  `tahun` year(4) DEFAULT NULL,
  `bulan` int(2) DEFAULT NULL,
  `nilaicf` double DEFAULT NULL,
  `nilaisf` double DEFAULT NULL,
  `nilairanking` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table thesis.hasilnilairanking: ~2 rows (approximately)
/*!40000 ALTER TABLE `hasilnilairanking` DISABLE KEYS */;
INSERT INTO `hasilnilairanking` (`kode`, `tahun`, `bulan`, `nilaicf`, `nilaisf`, `nilairanking`) VALUES
	('K001', '2018', 12, 3.1666666666667, 4.0714285714286, 3.8904761904762),
	('K002', '2018', 12, 2.6666666666667, 3.3214285714286, 3.2279761904762);
/*!40000 ALTER TABLE `hasilnilairanking` ENABLE KEYS */;

-- Dumping structure for table thesis.nilaikompetensi
CREATE TABLE IF NOT EXISTS `nilaikompetensi` (
  `id_kompetensi` int(11) NOT NULL AUTO_INCREMENT,
  `selisihgap` int(11) DEFAULT NULL,
  `bobonilai` float DEFAULT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_kompetensi`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table thesis.nilaikompetensi: ~8 rows (approximately)
/*!40000 ALTER TABLE `nilaikompetensi` DISABLE KEYS */;
INSERT INTO `nilaikompetensi` (`id_kompetensi`, `selisihgap`, `bobonilai`, `keterangan`) VALUES
	(1, 0, 5, 'Kompetensi sesuai yang dibutuhkan'),
	(2, 1, 4.5, 'Kompetensi kelebihan 1 tingkat/level'),
	(3, -1, 4, 'Kompetensi kekurangan 1 tingkat/level'),
	(4, 2, 3.5, 'Kompetensi kelebihan 2 tingkat/level'),
	(5, -2, 3, 'Kompetensi kekurangan 2 tingkat/level'),
	(6, 3, 2.5, 'Kompetensi kelebihan 3 tingkat/level'),
	(7, -3, 2, 'Kompetensi kekurangan 3 tingkat/level'),
	(8, 4, 1.5, 'Kompetensi kelebihan 4 tingkat/level'),
	(9, -4, 1, 'Kompetensi kekurangan 4 tingkat/level');
/*!40000 ALTER TABLE `nilaikompetensi` ENABLE KEYS */;

-- Dumping structure for table thesis.nilaipf
CREATE TABLE IF NOT EXISTS `nilaipf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kriteria` varchar(10) DEFAULT NULL,
  `nilai` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_kriteria` (`id_kriteria`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Dumping data for table thesis.nilaipf: ~15 rows (approximately)
/*!40000 ALTER TABLE `nilaipf` DISABLE KEYS */;
INSERT INTO `nilaipf` (`id`, `id_kriteria`, `nilai`) VALUES
	(1, 'EDU', 3),
	(2, 'VI', 3),
	(3, 'RRS', 4),
	(4, 'CON', 5),
	(5, 'CI', 5),
	(6, 'ACP', 3),
	(7, 'YS', 4),
	(8, 'AR', 4),
	(9, 'BVR', 4),
	(10, 'WA', 4),
	(11, 'VP', 3),
	(12, 'DC', 3),
	(13, 'ATC', 3),
	(14, 'QW', 3),
	(15, 'WS', 3),
	(16, 'RI', 3),
	(17, 'CTV', 3);
/*!40000 ALTER TABLE `nilaipf` ENABLE KEYS */;

-- Dumping structure for table thesis.t_rule
CREATE TABLE IF NOT EXISTS `t_rule` (
  `id_kriteria` varchar(3) DEFAULT NULL,
  `kriteria` varchar(10) DEFAULT NULL,
  UNIQUE KEY `id_kriteria` (`id_kriteria`),
  UNIQUE KEY `kriteria` (`kriteria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table thesis.t_rule: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_rule` DISABLE KEYS */;
INSERT INTO `t_rule` (`id_kriteria`, `kriteria`) VALUES
	('K1', 'EDU');
/*!40000 ALTER TABLE `t_rule` ENABLE KEYS */;

-- Dumping structure for table thesis.t_rule_param
CREATE TABLE IF NOT EXISTS `t_rule_param` (
  `id_rule_param` int(11) NOT NULL AUTO_INCREMENT,
  `id_kriteria` varchar(10) NOT NULL DEFAULT '0',
  `range` int(11) DEFAULT NULL,
  `Y0` int(11) DEFAULT NULL,
  `Y1` int(11) DEFAULT NULL,
  `X0` int(11) DEFAULT NULL,
  `X1` int(11) DEFAULT NULL,
  `goodness` int(11) DEFAULT NULL,
  `cond` text,
  PRIMARY KEY (`id_rule_param`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=latin1;

-- Dumping data for table thesis.t_rule_param: ~140 rows (approximately)
/*!40000 ALTER TABLE `t_rule_param` DISABLE KEYS */;
INSERT INTO `t_rule_param` (`id_rule_param`, `id_kriteria`, `range`, `Y0`, `Y1`, `X0`, `X1`, `goodness`, `cond`) VALUES
	(1, 'EDU', 1, 1, 0, 1, 1, 1, 'x <= 1'),
	(2, 'EDU', 2, 0, 1, 1, 2, 1, 'x > 1 && x < 2'),
	(3, 'EDU', 3, 1, 0, 2, 3, 2, 'x == 2'),
	(4, 'EDU', 2, 1, 0, 1, 2, NULL, 'x > 1 && x < 2'),
	(5, 'EDU', 4, 0, 1, 2, 3, 2, 'x > 2 && x < 3'),
	(6, 'EDU', 4, 1, 0, 2, 3, NULL, 'x > 2 && x < 3'),
	(7, 'EDU', 5, 1, 1, 3, 3, 3, 'x >= 3'),
	(8, 'VI', 1, 1, 0, 5, 5, 1, 'x <= 5'),
	(9, 'VI', 2, 0, 1, 5, 10, 1, 'x > 5 && x < 10'),
	(11, 'VI', 2, 1, 0, 5, 10, NULL, 'x > 5 && x < 10'),
	(12, 'VI', 3, 1, 0, 10, 15, 2, 'x == 10'),
	(13, 'VI', 4, 0, 1, 10, 15, 2, 'x > 10 && x < 15'),
	(14, 'VI', 4, 1, 0, 10, 15, NULL, 'x > 10 && x < 15'),
	(15, 'VI', 5, 1, 1, 15, 15, 3, 'x >= 15'),
	(16, 'RRS', 1, 1, 0, 1, 1, 1, 'x <= 1'),
	(17, 'RRS', 2, 0, 1, 1, 2, 2, 'x > 1 && x < 2'),
	(18, 'RRS', 2, 1, 0, 1, 2, NULL, 'x > 1 && x < 2'),
	(19, 'RRS', 3, 1, 0, 2, 3, 3, 'x == 2'),
	(20, 'RRS', 4, 0, 1, 2, 3, 4, 'x > 2 && x < 3'),
	(21, 'RRS', 4, 1, 0, 2, 3, NULL, 'x > 2 && x < 3'),
	(22, 'RRS', 5, 1, 1, 3, 4, 5, 'x >= 3'),
	(23, 'RRS', 6, 0, 1, 3, 4, 6, 'x > 3 && x < 4'),
	(24, 'RRS', 6, 1, 0, 3, 4, NULL, 'x > 3 && x < 4'),
	(25, 'RRS', 7, 1, 1, 4, 4, 7, 'x >= 4'),
	(26, 'CON', 1, 1, 0, 50, 50, 1, 'x <= 50'),
	(27, 'CON', 2, 0, 1, 50, 100, 2, 'x > 50 && x < 100'),
	(28, 'CON', 2, 1, 0, 50, 100, NULL, 'x > 50 && x < 100'),
	(29, 'CON', 3, 1, 0, 100, 150, 3, 'x == 100'),
	(30, 'CON', 4, 0, 1, 100, 150, 4, 'x > 100 && x < 150'),
	(31, 'CON', 4, 1, 0, 100, 150, NULL, 'x > 100 && x < 150'),
	(32, 'CON', 5, 1, 1, 150, 200, 5, 'x >= 150'),
	(33, 'CON', 6, 0, 1, 150, 200, 6, 'x > 150 && x < 200'),
	(34, 'CON', 6, 1, 0, 150, 200, 6, 'x > 150 && x < 200'),
	(35, 'CON', 7, 1, 0, 200, 250, 7, 'x == 200'),
	(36, 'CON', 8, 0, 1, 200, 250, 8, 'x > 200 && x < 250'),
	(37, 'CON', 8, 1, 0, 200, 250, NULL, 'x > 200 && x < 250'),
	(38, 'CON', 9, 1, 1, 250, 250, 9, 'x >= 250'),
	(39, 'CI', 1, 1, 0, 1, 1, 1, 'x <= 1'),
	(40, 'CI', 2, 0, 1, 1, 2, 2, 'x > 1 && x < 2'),
	(41, 'CI', 2, 1, 0, 1, 2, NULL, 'x > 1 && x < 2'),
	(42, 'CI', 3, 1, 0, 2, 3, 3, 'x == 2'),
	(43, 'CI', 4, 0, 1, 2, 3, 4, 'x > 2 && x < 3'),
	(44, 'CI', 4, 1, 0, 2, 3, NULL, 'x > 2 && x < 3'),
	(45, 'CI', 5, 1, 1, 3, 4, 5, 'x >= 3'),
	(46, 'CI', 6, 0, 1, 3, 4, 6, 'x > 3 && x < 4'),
	(47, 'CI', 6, 1, 0, 3, 4, NULL, 'x > 3 && x < 4'),
	(48, 'CI', 7, 1, 0, 4, 5, 7, 'x == 4'),
	(49, 'CI', 8, 0, 1, 4, 5, 8, 'x > 4 && x < 5'),
	(50, 'CI', 8, 1, 0, 4, 5, NULL, 'x > 4 && x < 5'),
	(51, 'CI', 9, 1, 1, 5, 5, 9, 'x >= 5'),
	(52, 'ACP', 1, 1, 0, 1, 1, 1, 'x <= 1'),
	(53, 'ACP', 2, 0, 1, 1, 3, 1, 'x > 1 && x < 3'),
	(54, 'ACP', 2, 1, 0, 1, 3, NULL, 'x > 1 && x < 3'),
	(55, 'ACP', 3, 1, 0, 3, 5, 2, 'x == 3'),
	(56, 'ACP', 4, 0, 1, 3, 5, 2, 'x > 3 && x < 5'),
	(57, 'ACP', 4, 1, 0, 3, 5, NULL, 'x > 3 && x < 5'),
	(58, 'ACP', 5, 1, 1, 5, 5, 3, 'x >= 5'),
	(59, 'YS', 1, 1, 0, 1, 1, 1, 'x <= 1'),
	(60, 'YS', 2, 0, 1, 1, 2, 2, 'x > 1 && x < 2'),
	(61, 'YS', 2, 1, 0, 1, 2, NULL, 'x > 1 && x < 2'),
	(62, 'YS', 3, 1, 0, 2, 3, 3, 'x == 2'),
	(63, 'YS', 4, 0, 1, 2, 3, 4, 'x > 2 && x < 3'),
	(64, 'YS', 4, 1, 0, 2, 3, NULL, 'x > 2 && x < 3'),
	(65, 'YS', 5, 1, 1, 3, 4, 5, 'x >= 3'),
	(66, 'YS', 6, 0, 1, 3, 4, 6, 'x > 3 && x < 4'),
	(67, 'YS', 6, 1, 0, 3, 4, NULL, 'x > 3 && x < 4'),
	(68, 'YS', 7, 1, 1, 4, 4, 7, 'x >= 4'),
	(69, 'AR', 1, 1, 0, 20, 20, 1, 'x <= 20'),
	(70, 'AR', 2, 0, 1, 20, 40, 2, 'x > 20 && x < 40'),
	(71, 'AR', 2, 1, 0, 20, 40, NULL, 'x > 20 && x < 40'),
	(72, 'AR', 3, 1, 0, 40, 60, 3, 'x == 40'),
	(73, 'AR', 4, 0, 1, 40, 60, 4, 'x > 40 && x < 60'),
	(74, 'AR', 4, 1, 0, 40, 60, NULL, 'x > 40 && x < 60'),
	(75, 'AR', 5, 1, 1, 60, 80, 5, 'x >= 60'),
	(76, 'AR', 6, 0, 1, 60, 80, 6, 'x > 60 && x < 80'),
	(77, 'AR', 6, 1, 0, 60, 80, NULL, 'x > 60 && x < 80'),
	(78, 'AR', 7, 1, 1, 80, 80, 7, 'x >= 80'),
	(79, 'BVR', 1, 1, 0, 7, 7, 1, 'x <= 7'),
	(80, 'BVR', 2, 0, 1, 7, 14, 2, 'x > 7 && x < 14'),
	(81, 'BVR', 2, 1, 0, 7, 14, NULL, 'x > 7 && x < 14'),
	(82, 'BVR', 3, 1, 0, 14, 21, 3, 'x == 14'),
	(83, 'BVR', 4, 0, 1, 14, 21, 4, 'x > 14 && x < 21'),
	(84, 'BVR', 4, 1, 0, 14, 21, NULL, 'x > 14 && x < 21'),
	(85, 'BVR', 5, 1, 1, 21, 28, 5, 'x >= 21'),
	(86, 'BVR', 6, 0, 1, 21, 28, 6, 'x > 21 && x < 28'),
	(87, 'BVR', 6, 1, 0, 21, 28, NULL, 'x > 21 && x < 28'),
	(88, 'BVR', 7, 1, 1, 28, 28, 7, 'x >= 28'),
	(89, 'WA', 1, 1, 0, 20, 20, 1, 'x <= 20'),
	(90, 'WA', 2, 0, 1, 20, 40, 2, 'x > 20 && x < 40'),
	(91, 'WA', 2, 1, 0, 20, 40, NULL, 'x > 20 && x < 40'),
	(92, 'WA', 3, 1, 0, 40, 60, 3, 'x == 40'),
	(93, 'WA', 4, 0, 1, 40, 60, 4, 'x > 40 && x < 60'),
	(94, 'WA', 4, 1, 0, 40, 60, NULL, 'x > 40 && x < 60'),
	(95, 'WA', 5, 1, 1, 60, 80, 5, 'x >= 60'),
	(96, 'WA', 6, 0, 1, 60, 80, 6, 'x > 60 && x < 80'),
	(97, 'WA', 6, 1, 0, 60, 80, NULL, 'x > 60 && x < 80'),
	(98, 'WA', 7, 1, 1, 80, 80, 7, 'x >= 80'),
	(99, 'VP', 1, 1, 0, 1, 1, 1, 'x <= 1'),
	(100, 'VP', 2, 0, 1, 1, 3, 1, 'x > 1 && x < 3'),
	(101, 'VP', 2, 1, 0, 1, 3, NULL, 'x > 1 && x < 3'),
	(102, 'VP', 3, 1, 0, 3, 5, 2, 'x == 3'),
	(103, 'VP', 4, 0, 1, 3, 5, 2, 'x > 3 && x < 5'),
	(104, 'VP', 4, 1, 0, 3, 5, NULL, 'x > 3 && x < 5'),
	(105, 'VP', 5, 1, 1, 5, 5, 3, 'x >= 5'),
	(106, 'DC', 1, 1, 0, 1, 1, 1, 'x <= 1'),
	(107, 'DC', 2, 0, 1, 1, 2, 1, 'x > 1 && x < 2'),
	(109, 'DC', 2, 1, 0, 1, 2, NULL, 'x > 1 && x < 2'),
	(110, 'DC', 3, 1, 0, 2, 3, 2, 'x == 2'),
	(111, 'DC', 4, 0, 1, 2, 3, 2, 'x > 2 && x < 3'),
	(112, 'DC', 4, 1, 0, 2, 3, NULL, 'x > 2 && x < 3'),
	(113, 'DC', 5, 1, 1, 3, 3, 3, 'x >= 3'),
	(114, 'ATC', 1, 1, 0, 50, 50, 1, 'x <= 50'),
	(115, 'ATC', 2, 0, 1, 50, 100, 1, 'x > 50 && x < 100'),
	(116, 'ATC', 2, 1, 0, 50, 100, NULL, 'x > 50 && x < 100'),
	(117, 'ATC', 3, 1, 0, 100, 150, 2, 'x == 100'),
	(118, 'ATC', 4, 0, 1, 100, 150, 2, 'x > 100 && x < 150'),
	(119, 'ATC', 4, 1, 0, 100, 150, NULL, 'x > 100 && x < 150'),
	(120, 'ATC', 5, 1, 1, 150, 150, 3, 'x >= 150'),
	(121, 'QW', 1, 1, 0, 1, 1, 1, 'x <= 1'),
	(122, 'QW', 2, 0, 1, 1, 2, 1, 'x > 1 && x < 2'),
	(123, 'QW', 2, 1, 0, 1, 2, NULL, 'x > 1 && x < 2'),
	(124, 'QW', 3, 1, 0, 2, 3, 2, 'x == 2'),
	(125, 'QW', 4, 0, 1, 2, 3, 2, 'x > 2 && x < 3'),
	(126, 'QW', 4, 1, 0, 2, 3, NULL, 'x > 2 && x < 3'),
	(127, 'QW', 5, 1, 1, 3, 3, 3, 'x >= 3'),
	(128, 'WS', 1, 1, 0, 1, 1, 1, 'x <= 1'),
	(129, 'WS', 2, 0, 1, 1, 2, 1, 'x > 1 && x < 2'),
	(130, 'WS', 2, 1, 0, 1, 2, NULL, 'x > 1 && x < 2'),
	(131, 'WS', 3, 1, 0, 2, 3, 2, 'x == 2'),
	(132, 'WS', 4, 0, 1, 2, 3, 2, 'x > 2 && x < 3'),
	(133, 'WS', 4, 1, 0, 2, 3, NULL, 'x > 2 && x < 3'),
	(134, 'WS', 5, 1, 1, 3, 3, 3, 'x >= 3'),
	(135, 'RI', 1, 1, 0, 1, 1, 1, 'x <= 1'),
	(136, 'RI', 2, 0, 1, 1, 2, 1, 'x > 1 && x < 2'),
	(137, 'RI', 2, 1, 0, 1, 2, NULL, 'x > 1 && x < 2'),
	(138, 'RI', 3, 1, 0, 2, 3, 2, 'x == 2'),
	(139, 'RI', 4, 0, 1, 2, 3, 2, 'x > 2 && x < 3'),
	(140, 'RI', 4, 1, 0, 2, 3, NULL, 'x > 2 && x < 3'),
	(141, 'RI', 5, 1, 1, 3, 3, 3, 'x >= 3'),
	(142, 'CTV', 1, 1, 0, 1, 1, 1, 'x <= 1'),
	(143, 'CTV', 2, 0, 1, 1, 2, 1, 'x > 1 && x < 2'),
	(144, 'CTV', 2, 1, 0, 1, 2, NULL, 'x > 1 && x < 2'),
	(145, 'CTV', 3, 1, 0, 2, 3, 2, 'x == 2'),
	(146, 'CTV', 4, 0, 1, 2, 3, 2, 'x > 2 && x < 3'),
	(147, 'CTV', 4, 1, 0, 2, 3, NULL, 'x > 2 && x < 3'),
	(148, 'CTV', 5, 1, 1, 3, 3, 3, 'x >= 3');
/*!40000 ALTER TABLE `t_rule_param` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
