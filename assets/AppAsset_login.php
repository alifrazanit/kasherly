<?php
namespace app\assets;
use yii\web\AssetBundle;
class AppAsset_login extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
		'theme/bower_components/bootstrap/dist/css/bootstrap.min.css',
		'theme/bower_components/font-awesome/css/font-awesome.min.css',
		'theme/bower_components/Ionicons/css/ionicons.min.css',
		'theme/dist/css/AdminLTE.min.css',
		'theme/plugins/iCheck/square/blue.css'
	];
    public $js = [
		'theme/plugins/iCheck/icheck.min.js',
		'theme/bower_components/bootstrap/dist/js/bootstrap.min.js',
		'theme/bower_components/jquery/dist/jquery.min.js',
		'js/jquery-3.4.1.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
