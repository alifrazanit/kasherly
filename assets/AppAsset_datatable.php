<?php
namespace app\assets;
use yii\web\AssetBundle;
class AppAsset_datatable extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
		'theme/bower_components/bootstrap/dist/css/bootstrap.min.css',
		'theme/bower_components/font-awesome/css/font-awesome.min.css',
		'theme/bower_components/Ionicons/css/ionicons.min.css',
		'theme/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
		'theme/dist/css/AdminLTE.min.css',
		'theme/dist/css/skins/_all-skins.min.css'
	];
    public $js = [
		'theme/bower_components/jquery/dist/jquery.min.js',
		'theme/bower_components/bootstrap/dist/js/bootstrap.min.js',
		'theme/bower_components/datatables.net/js/jquery.dataTables.min.js',
		'theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
		'theme/bower_components/jquery-slimscroll/jquery.slimscroll.min.js',
		'theme/bower_components/fastclick/lib/fastclick.js',
		'theme/dist/js/adminlte.min.js',
		'theme/dist/js/demo.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
