<?php

namespace app\assets;

use yii\web\AssetBundle;

class AppAsset_datatableadv extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
       'theme/bower_components/bootstrap/dist/css/bootstrap.min.css',
       'theme/bower_components/font-awesome/css/font-awesome.min.css',
       'theme/bower_components/Ionicons/css/ionicons.min.css',
       'theme/bower_components/bootstrap-daterangepicker/daterangepicker.css',
       'theme/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
       'theme/plugins/iCheck/all.css',
       'theme/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css',
       'theme/plugins/timepicker/bootstrap-timepicker.min.css',
       'theme/bower_components/select2/dist/css/select2.min.css',
       'theme/dist/css/AdminLTE.min.css',
       'theme/dist/css/skins/_all-skins.min.css',
	   'theme/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
    ];
    public $js = [
        'theme/bower_components/jquery/dist/jquery.min.js',
        'theme/bower_components/bootstrap/dist/js/bootstrap.min.js',
        'theme/bower_components/select2/dist/js/select2.full.min.js',
        'theme/plugins/input-mask/jquery.inputmask.js',
        'theme/plugins/input-mask/jquery.inputmask.date.extensions.js',
        'theme/plugins/input-mask/jquery.inputmask.extensions.js',
        'theme/bower_components/moment/min/moment.min.js',
        'theme/bower_components/bootstrap-daterangepicker/daterangepicker.js',
        'theme/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
        'theme/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js',
        'theme/plugins/timepicker/bootstrap-timepicker.min.js',
        'theme/bower_components/jquery-slimscroll/jquery.slimscroll.min.js',
        'theme/plugins/iCheck/icheck.min.js',
        'theme/bower_components/fastclick/lib/fastclick.js',
        'theme/dist/js/adminlte.min.js',
        'theme/dist/js/demo.js',
		'theme/bower_components/datatables.net/js/jquery.dataTables.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
