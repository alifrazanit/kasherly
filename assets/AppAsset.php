<?php
namespace app\assets;
use yii\web\AssetBundle;
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
		'theme/bower_components/bootstrap/dist/css/bootstrap.min.css',
		'theme/bower_components/font-awesome/css/font-awesome.min.css',
		'theme/bower_components/Ionicons/css/ionicons.min.css',
		'theme/dist/css/AdminLTE.min.css',
		'theme/dist/css/skins/_all-skins.min.css',
		'theme/bower_components/morris.js/morris.css',
		'theme/bower_components/jvectormap/jquery-jvectormap.css',
		'theme/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
		'theme/bower_components/bootstrap-daterangepicker/daterangepicker.css',
		'theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'
	];
    public $js = [
		'theme/bower_components/jquery/dist/jquery.min.js',
		'theme/bower_components/jquery-ui/jquery-ui.min.js',
		'theme/bower_components/bootstrap/dist/js/bootstrap.min.js',
		'theme/bower_components/raphael/raphael.min.js',
		'theme/bower_components/morris.js/morris.min.j',
		'theme/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js',
		'theme/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
		'theme/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
		'theme/bower_components/jquery-knob/dist/jquery.knob.min.js',
		'theme/bower_components/moment/min/moment.min.js',
		'theme/bower_components/bootstrap-daterangepicker/daterangepicker.j',
		'theme/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
		'theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
		'theme/bower_components/jquery-slimscroll/jquery.slimscroll.min.js',
		'theme/bower_components/fastclick/lib/fastclick.js',
		'theme/dist/js/adminlte.min.js',
		'theme/dist/js/pages/dashboard.js',
		'theme/dist/js/demo.js',
		'theme/bower_components/morris.js/morris.min.js',
		'theme/bower_components/raphael/raphael.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
